# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Considering data-status="" for easier status updates.
- Reconsidering use of `<time>` HTML tags, for machine-readable format may not be needed.
- Consider dropping CSS reset, for needed properties (font, line-height, etc.) are already set.
- Considering wether 'scheduled/unscheduled' or 'planned/unplanned' is clearer word choice.
- Venacular (regex): was (slow|down) for .* minutes due to maintenance on
	- [“Highrise was down for 3 minutes due to a database slowdown.”](https://www.basecampstatus.com/incidents/n7yg91vdmgk5)

# [0.3.1] - 2020-09-09
### Changed
- Began using 'down'/'downtime' over 'unavailable' to describe maintenance or outages.

# [0.3.0] - 2020-07-15
### Changed
- Reorganized styles and simplified semantic HTML with [Object Oriented CSS](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Organizing#OOCSS)

# [0.2.2] - 2020-07-12
### Changed
- Corrected HTTP ‘X-Content-Type-Options’ header to be served on all requests.
- [Rervereted] Removed: Content-Security-Policy reporting.

# [0.2.1] - 2020-07-06
### Removed
- X-XSS-Protection reporting (“[Deprecating XSS Reports](https://scotthelme.co.uk/deprecating-xss-reports/)”).
- Content-Security-Policy reporting.

# [0.2.0] - 2020-07-06
### Changed 
- Updated modern-normalize to [v0.7.0](https://github.com/sindresorhus/modern-normalize/releases/tag/v0.7.0)
- Removed `<abbr>` HTML tags, for note on time zone in footer, as they are not accessable on mobile devices: https://utcc.utoronto.ca/~cks/space/blog/web/HTMLAbbrAndMobileBrowsers
### Added
- Applied a maximum height (with scrolling) for archiving older maintenance/outage events.
### Removed
- “Custom Client Hosting” entry.

## [0.1.4] - 2020-05-13
### Added
- “Custom Client Hosting” entry.

## [0.1.3] - 2020-03-11
### Added
- Summary section, to consolidate maintenance notices from each status line.
### Changed
- Updated Bourbon framework to v7.0.0
- Reorganized balance of base and 'app' scss files.
- Reorganized tracked services.

## [0.1.2] - 2020-02-12
### Fixed
- Corrected .svg minification to include copyright notice, as exemption to CC-BY-SA 4.0.

## [0.1.1] - 2020-01-27
### Added
- Included copyright notice in .svg logo files, as exemption to CC-BY-SA 4.0.

## [0.1.0] - 2019-11-17
### Changed
- Changed license to more appropiate CC-BY-SA 4.0
- Consolidated COPYING.md into README.md
- Consolidated CONTRIBUTING.md into README.md

## [0.0.5] - 2019-07-27
### Changed
- Added README.md file.
- Renamed LICENSE.md to COPYING.md, to match recommendation [here](https://www.gnu.org/licenses/gpl-howto.html).

## [0.0.4] - 2019-06-24
### Changed
- Removed use of obsolete "All rights reserved" notice from footer.

## [0.0.3] - 2019-06-12
### Changed
- Moved Bourbon and Modern-normalize SCSS libraries to a CodeKit framework, to simplify project.

## [0.0.2] - 2019-03-13
### Changed
- Merged ‘Services’ and ‘Websites’ sections.

## [0.0.1] - 2019-03-13
### Added
- Added this CHANGELOG.md file.[^1]
