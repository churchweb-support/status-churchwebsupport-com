# Churchweb Support - System Status
[status.churchwebsupport.com](https://status.churchwebsupport.com) website, built with [CodeKit](https://codekitapp.com) and deployed with [Netlify](https://www.netlify.com).

The format is based on [Make a README](https://www.makeareadme.com).

## Contributing
Please review our License and include as much details as possible (such as web browser or other software). Thank you!

## License
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">System Status</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://www.churchwebsupport.com" property="cc:attributionName" rel="cc:attributionURL">Churchweb Support Inc.</a> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
